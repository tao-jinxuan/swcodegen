#ifndef SLAVE_TREE_H
#define SLAVE_TREE_H

#include <isl/schedule_node.h>

#include "slave.h"

__isl_give isl_schedule_node *slave_tree_insert_copy_before_thread(
	__isl_take isl_schedule_node *node);
int slave_tree_node_is_kernel(__isl_keep isl_schedule_node *node);
__isl_give isl_schedule_node *slave_tree_move_down_to_copy(
	__isl_take isl_schedule_node *node, __isl_keep isl_union_set *core);
__isl_give isl_schedule_node *slave_tree_move_up_to_thread(
	__isl_take isl_schedule_node *node);
__isl_give isl_schedule_node *slave_tree_move_down_to_thread(
	__isl_take isl_schedule_node *node, __isl_keep isl_union_set *core);
__isl_give isl_schedule_node *slave_tree_move_up_to_kernel(
	__isl_take isl_schedule_node *node);
__isl_give isl_schedule_node *slave_tree_move_down_to_depth(
	__isl_take isl_schedule_node *node, int depth,
	__isl_keep isl_union_set *core);

int slave_tree_id_is_sync(__isl_keep isl_id *id, struct ppcg_kernel *kernel);
__isl_give isl_schedule_node *slave_tree_ensure_sync_after_core(
	__isl_take isl_schedule_node *node, struct ppcg_kernel *kernel);
__isl_give isl_schedule_node *slave_tree_ensure_following_sync(
	__isl_take isl_schedule_node *node, struct ppcg_kernel *kernel);
__isl_give isl_schedule_node *slave_tree_move_left_to_sync(
	__isl_take isl_schedule_node *node, struct ppcg_kernel *kernel);
__isl_give isl_schedule_node *slave_tree_move_right_to_sync(
	__isl_take isl_schedule_node *node, struct ppcg_kernel *kernel);

#endif
