#ifndef _SW_COMMON_H_
#define _SW_COMMON_H_

#include <stdio.h>

struct sw_info {
	FILE *host_c;
	FILE *slave_c;
	FILE *slave_h;
};

void sw_open_files(struct sw_info *info, const char *input);
void sw_close_files(struct sw_info *info);

#endif
