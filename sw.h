#ifndef _SW_H
#define _SW_H

#include "ppcg.h"
#include "ppcg_options.h"

int generate_sw(isl_ctx *ctx, struct ppcg_options *options,
	const char *input);

#endif
