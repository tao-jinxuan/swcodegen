#include <ctype.h>
#include <limits.h>
#include <string.h>

#include "sw_common.h"
#include "ppcg.h"

/* Open the hosc .c file and the slave .c file for writing.
 * Add the neccessary includes.
 */
void sw_open_files(struct sw_info *info, const char *input)
{
	char name[PATH_MAX];
	int len;

	len = ppcg_extract_base_name(name, input);

	strcpy(name + len, "_host.c");
	info->host_c = fopen(name, "w");

	strcpy(name + len, "_slave.c");
	info->slave_c = fopen(name, "w");

	strcpy(name + len, "_slave.h");
	info->slave_h = fopen(name, "w");

	fprintf(info->host_c, "#include <stdlib.h>\n");
	//fprintf(info->host_c, "#include <stdio.h>\n");
	fprintf(info->host_c, "#include <athread.h>\n");
	//fprintf(info->host_c, "#include <sys/type.h>\n");
	//fprintf(info->host_c, "#include <sys/stat.h>\n");
	fprintf(info->host_c, "#include <fcntl.h>\n");
	fprintf(info->host_c, "#include \"%s\"\n", name);
	fprintf(info->slave_c, "#include <stdio.h>\n");
	fprintf(info->slave_c, "#include <math.h>\n");
	fprintf(info->slave_c, "#include <string.h>\n");
	fprintf(info->slave_c, "#include \"slave.h\"\n");
}

/* Close all output files.
 */
void sw_close_files(struct sw_info *info)
{
	fclose(info->slave_h);
	fclose(info->slave_c);
	fclose(info->host_c);
}
