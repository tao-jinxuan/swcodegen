#ifndef SLAVE_PRINT_H
#define SLAVE_PRINT_H

#include "slave.h"

__isl_give isl_printer *slave_print_local_declarations(__isl_take isl_printer *p,
	struct slave_prog *prog);

__isl_give isl_printer *slave_print_types(__isl_take isl_printer *p,
	struct slave_types *types, struct slave_prog *prog);

__isl_give isl_printer *slave_print_macros(__isl_take isl_printer *p,
	__isl_keep isl_ast_node *node);

__isl_give isl_printer *slave_array_info_print_size(__isl_take isl_printer *prn,
	struct slave_array_info *array);
__isl_give isl_printer *slave_array_info_print_declaration_argument(
	__isl_take isl_printer *p, struct slave_array_info *array,
	const char *memory_space);
__isl_give isl_printer *slave_array_info_print_call_argument(
	__isl_take isl_printer *p, struct slave_array_info *array);

__isl_give isl_printer *ppcg_kernel_print_copy_for_sw(__isl_take isl_printer *p,
	struct ppcg_kernel_stmt *stmt);
__isl_give isl_printer *ppcg_kernel_print_domain_for_sw(__isl_take isl_printer *p,
	struct ppcg_kernel_stmt *stmt);

__isl_give isl_printer *stmt_print_local_index(__isl_take isl_printer *p,
   	struct ppcg_kernel_stmt *stmt);
__isl_give isl_printer *stmt_print_global_index(
  	__isl_take isl_printer *p, struct ppcg_kernel_stmt *stmt);
#endif
